CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Maintainers


INTRODUCTION
------------

Password Policy Configuration module for overriding password field validation using matching patterns.


CONFIGURATION
-------------

 admin/config/passwordpolicyconfiguration
 * Matching pattern configuration
 * Exclude Admin User
 * Force Validation


MAINTAINERS
-----------

Current maintainers:
 * Rahul Chauhan (rahulchauhan_ec3) - https://www.drupal.org/u/rahulchauhan_ec3
 * Dinesh Saini (dineshsaini) - https://www.drupal.org/u/dineshsaini
 * Surya Prakash Gangwar (gangwarsurya) - https://www.drupal.org/u/gangwarsurya
 * Praveen Paliya (ppaliya) - https://www.drupal.org/u/ppaliya


This project has been sponsored by:
 * Xeliumtech Solutions Pvt. Ltd.
   Founded in 2010, XeliumTech Solutions, a member of Nasscom, Started in 2010 
   with a vision to be a quality one-stop solutions company. Today, we are 
   proud to be a end to end trusted Software services partners for companies 
   globally across North America, Australia, UK,  Europe, Middle-East, 
   South-East Asia. Visit: https://www.drupal.org/xeliumtech-solutions-pvt-ltd 
   for more information.
